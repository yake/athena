/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#pragma message( "The package L1CommonCore is obsolete and will be removed" )
#include "CTPfragment/CTPdataformatVersion.h"
